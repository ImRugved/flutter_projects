import 'package:flutter/material.dart';

class NewScreen extends StatefulWidget {
  NewScreen({Key? key}) : super(key: key);

  @override
  _NewScreenState createState() => _NewScreenState();
}

class _NewScreenState extends State<NewScreen> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _descriptionFocusNode = FocusNode();
  final FocusNode _dateFocusNode = FocusNode();
  final List<String> _enteredNameValue = [];
  final List<String> _enteredDescriptionValue = [];
  final List<String> _enteredDateValue = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('insta'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(height: 60),
            SizedBox(
              width: 300,
              child: TextField(
                controller: _nameController,
                focusNode: _nameFocusNode,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Enter Your Name",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(color: Colors.pink),
                  ),
                ),
                cursorColor: Colors.black,
                textInputAction: TextInputAction.done,
                onChanged: (value) {
                  print('Value is: $value');
                },
                onSubmitted: (value) {
                  print('sumtted val is :$value');
                },
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: 300,
              child: TextField(
                controller: _descriptionController,
                focusNode: _descriptionFocusNode,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Enter Your Name",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(color: Colors.pink),
                  ),
                ),
                cursorColor: Colors.black,
                textInputAction: TextInputAction.done,
                onChanged: (value) {
                  print('Value is: $value');
                },
                onSubmitted: (value) {
                  print('sumtted val is :$value');
                },
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: 300,
              child: TextField(
                controller: _dateController,
                focusNode: _dateFocusNode,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Enter Your Name",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(color: Colors.pink),
                  ),
                ),
                cursorColor: Colors.black,
                textInputAction: TextInputAction.done,
                onChanged: (value) {
                  print('Value is: $value');
                },
                onSubmitted: (value) {
                  print('sumtted val is :$value');
                },
              ),
            ),
            const SizedBox(height: 30),
            Expanded(
              child: ListView.builder(
                itemCount: _enteredNameValue.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      leading: Expanded(
                        child: Expanded(
                          child: CircleAvatar(
                            child: Image.network(
                              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTc1JzU1FDGrFNFDGFt6LylcJB7o18u9Ir0mg&s',
                            ),
                          ),
                        ),
                      ),
                      title: Text(
                        _enteredNameValue[index],
                      ),
                      subtitle: Text(_enteredDescriptionValue[index]),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: const Icon(Icons.edit),
                            onPressed: () {
                              // Implement edit functionality (modify existing task)
                              // ...
                            },
                          ),
                          IconButton(
                            icon: const Icon(Icons.delete),
                            onPressed: () {
                              setState(() {
                                // Remove task from lists and update UI
                                _enteredNameValue.removeAt(index);
                                _enteredDescriptionValue.removeAt(index);
                                _enteredDateValue.removeAt(index);
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _addNameToList();
          _addDescriptionToList();
          _addDateToList();
        },
        child: Icon(Icons.add),
      ),
    );
  }

  void _addNameToList() {
    setState(() {
      String enteredName = _nameController.text.trim();
      if (enteredName.isNotEmpty) {
        _enteredNameValue.add(enteredName);
        _nameController.clear();
      }
    });
  }

  void _addDescriptionToList() {
    setState(() {
      String enteredDiscription = _descriptionController.text.trim();
      if (enteredDiscription.isNotEmpty) {
        _enteredDescriptionValue.add(enteredDiscription);
        _descriptionController.clear();
      }
    });
  }

  void _addDateToList() {
    setState(() {
      String enteredDate = _dateController.text.trim();
      if (enteredDate.isNotEmpty) {
        _enteredDateValue.add(enteredDate);
        _dateController.clear();
      }
    });
  }
}
