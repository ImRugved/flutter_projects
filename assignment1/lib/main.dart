import 'package:assignment1/c2whome.dart';
import 'package:assignment1/home.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Palindrome',
      home: Assignment4(),
    );
  }
}
