import 'package:flutter/material.dart';

class HomePage1 extends StatefulWidget {
  const HomePage1({Key? key}) : super(key: key);

  @override
  State<HomePage1> createState() => _HomePage1State();
}

class _HomePage1State extends State<HomePage1> {
  int _palindromeCount = 0;
  int _strongCount = 0;
  int _armstrongCount = 0;

  final List<int> _numList = [
    1234,
    3456,
    121,
    232,
    111,
    133,
    145,
    153,
    370,
    371,
    2,
    1,
    871,
  ];

  void _checkPalindrome() {
    setState(() {
      _palindromeCount = 0;
      for (var i = 0; i < _numList.length; i++) {
        int temp = _numList[i].abs();
        int rev = 0;

        while (temp > 0) {
          rev = rev * 10 + temp % 10;
          temp ~/= 10;
        }

        if (_numList[i].abs() == rev) {
          _palindromeCount++;
        }
      }
    });
  }

  void _checkStrong() {
    setState(() {
      _strongCount = 0;

      for (int i = 0; i < _numList.length; i++) {
        int temp = _numList[i];
        int sum = 0;
        int num = temp;
        while (num != 0) {
          int rem = num % 10;
          int fact = 1;
          for (int j = 1; j <= rem; j++) {
            fact *= j;
          }
          sum += fact;
          num = num ~/ 10;
        }
        if (sum == _numList[i] && _numList.contains(sum)) {
          _strongCount++;
        }
      }
    });
  }

  void _checkArmstrong() {
    setState(() {
      _armstrongCount = 0;
      for (int i = 0; i < _numList.length; i++) {
        int temp = _numList[i];
        int temp1 = _numList[i];
        int cnt = 0;
        int sum = 0;

        while (temp1 != 0) {
          cnt++;
          temp1 = temp1 ~/ 10;
        }

        while (temp != 0) {
          int rem = temp % 10;
          int arm = 1;
          for (int num = 1; num <= cnt; num++) {
            arm *= rem;
          }
          sum += arm;
          temp = temp ~/ 10;
        }

        if (sum == _numList[i]) {
          _armstrongCount++;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text('Number Checker'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              alignment: Alignment.topCenter,
              height: 60,
              color: Colors.yellow,
              child: const Center(
                child: Text(
                  'Click buttons to show the counts  ',
                  style: TextStyle(fontSize: 30),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Column(
              children: [
                Container(
                  color: Colors.amber,
                  child: Column(
                    children: [
                      const Center(
                        child: Text(
                          'Count of palindrome numbers in the list :',
                          style: const TextStyle(fontSize: 20),
                        ),
                      ),
                      Text(
                        "$_palindromeCount",
                        style: const TextStyle(fontSize: 20),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        style: const ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(
                            Colors.red,
                          ),
                        ),
                        onPressed: () {
                          _checkPalindrome();
                        },
                        child: const Text(
                          'Palindrome',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  color: Colors.greenAccent,
                  child: Column(
                    children: [
                      const Center(
                        child: Text(
                          'Count of strong numbers in the list :',
                          style: const TextStyle(fontSize: 20),
                        ),
                      ),
                      Text(
                        "$_strongCount",
                        style: const TextStyle(fontSize: 20),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.blue),
                        ),
                        onPressed: () {
                          _checkStrong();
                        },
                        child: const Text(
                          'Strong',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  color: Colors.pinkAccent,
                  child: Column(
                    children: [
                      const Center(
                        child: Text(
                          'Count of armstrong numbers in the list :',
                          style: const TextStyle(fontSize: 20),
                        ),
                      ),
                      Text(
                        "$_armstrongCount",
                        style: const TextStyle(fontSize: 20),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.limeAccent),
                        ),
                        onPressed: () {
                          _checkArmstrong();
                        },
                        child: const Text(
                          'Armstrong',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              style: const ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(Colors.green)),
              onPressed: () {
                setState(() {
                  _palindromeCount = 0;
                  _strongCount = 0;
                  _armstrongCount = 0;
                });
              },
              child: const Text('Reset',
                  style: TextStyle(fontSize: 20, color: Colors.black)),
            ),
          ],
        ),
      ),
    );
  }
}
