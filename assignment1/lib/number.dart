import 'dart:io';

void main() {
  print('Enter a number:');
  int originalNumber = int.parse(stdin.readLineSync()!);

  // Check Palindrome
  int reversedPalindrome = 0;
  int numberPalindrome = originalNumber;

  while (numberPalindrome > 0) {
    int digit = numberPalindrome % 10;
    reversedPalindrome = reversedPalindrome * 10 + digit;
    numberPalindrome ~/= 10;
  }

  if (originalNumber == reversedPalindrome) {
    print('$originalNumber is a palindrome.');
  } else {
    print('$originalNumber is not a palindrome.');
  }

  // Check Armstrong
  int powerArmstrong = countDigits(originalNumber);
  int sumArmstrong = 0;
  int numberArmstrong = originalNumber;

  while (numberArmstrong > 0) {
    int digit = numberArmstrong % 10;
    sumArmstrong += pow(digit, powerArmstrong);
    numberArmstrong ~/= 10;
  }

  if (originalNumber == sumArmstrong) {
    print('$originalNumber is an Armstrong number.');
  } else {
    print('$originalNumber is not an Armstrong number.');
  }

  // Check Strong
  int sumStrong = 0;
  int numberStrong = originalNumber;

  while (numberStrong > 0) {
    int digit = numberStrong % 10;
    sumStrong += factorial(digit);
    numberStrong ~/= 10;
  }

  if (originalNumber == sumStrong) {
    print('$originalNumber is a Strong number.');
  } else {
    print('$originalNumber is not a Strong number.');
  }
}

int countDigits(int n) {
  int count = 0;
  while (n != 0) {
    count++;
    n ~/= 10;
  }
  return count;
}

int pow(int base, int exponent) {
  int result = 1;
  for (int i = 0; i < exponent; i++) {
    result *= base;
  }
  return result;
}

int factorial(int n) {
  if (n == 0 || n == 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}
