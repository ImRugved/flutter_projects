import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: const Color.fromRGBO(255, 19, 93, 1),
            // borderRadius: BorderRadius.circular(100),
            border: Border.all(
              color: const Color.fromARGB(255, 217, 255, 0),
              width: 5,
            ),
            boxShadow: const [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 1),
                offset: Offset(20, -20),
                blurRadius: 20,
              ),
            ],
          ),
          child: Image.network(
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFsrVc1_PNsDsVh3yii51X2Mvt6rZy7C026A&usqp=CAU',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
