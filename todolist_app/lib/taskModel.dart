class TaskModel {
  String title;
  String description;
  String date;

  TaskModel({
    required this.title,
    required this.description,
    required this.date,
  });
}
