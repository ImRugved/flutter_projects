import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:todolist_app/loginPage.dart';
import 'package:todolist_app/taskModel.dart';

class TodoApp extends StatefulWidget {
  const TodoApp({super.key});

  @override
  State<TodoApp> createState() => _TodoAppState();
}

class _TodoAppState extends State<TodoApp> {
  bool shadoColor = false;
  bool nightMode = false;
  var titleValidator = false;
  var descriptionValidator = false;
  var dateValidator = false;

  TextEditingController titleController = TextEditingController();

  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  void showBottomSheet(bool doEdit, [TaskModel? taskModelObj]) {
    showModalBottomSheet(
      backgroundColor: nightMode ? Colors.black : Colors.white,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      isDismissible: true,
      context: context,
      builder: (context) {
        return Padding(
          //padding: MediaQuery.of(context).viewInsets,
          padding: EdgeInsets.only(
            left: 20,
            right: 20,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              Text(
                'Create New Task',
                style: GoogleFonts.quicksand(
                  textStyle: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                    color: nightMode ? Colors.white : Colors.black,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Title',
                    style: TextStyle(
                        color: Color.fromRGBO(1, 177, 167, 2),
                        fontSize: 15,
                        fontWeight: FontWeight.w400),
                  ),
                  SizedBox(
                    width: 600,
                    child: TextField(
                      maxLength: 50,
                      controller: titleController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(1, 177, 167, 2),
                          ),
                        ),
                        errorText: titleValidator ? 'Enter Title' : null,
                      ),
                      cursorColor: Colors.black,
                      textInputAction: TextInputAction.done,
                    ),
                  ),
                  const Text(
                    'Description',
                    style: TextStyle(
                        color: Color.fromRGBO(1, 177, 167, 2),
                        fontSize: 15,
                        fontWeight: FontWeight.w400),
                  ),
                  SizedBox(
                    width: 600,
                    child: TextField(
                      maxLength: 300,
                      controller: descriptionController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(1, 177, 167, 2),
                          ),
                        ),
                        errorText: titleValidator ? 'Enter Description' : null,
                      ),
                      cursorColor: Colors.black,
                      textInputAction: TextInputAction.done,
                    ),
                  ),
                  const Text(
                    'Date',
                    style: TextStyle(
                        color: Color.fromRGBO(1, 177, 167, 2),
                        fontSize: 15,
                        fontWeight: FontWeight.w400),
                  ),
                  SizedBox(
                    width: 600,
                    child: TextField(
                      controller: dateController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(1, 177, 167, 2),
                          ),
                        ),
                        fillColor: const Color.fromRGBO(1, 177, 167, 2),
                        suffix: const Icon(
                          Icons.calendar_month_outlined,
                        ),
                        errorText: titleValidator ? 'Enter Date' : null,
                      ),
                      cursorColor: Colors.black,
                      textInputAction: TextInputAction.done,
                      readOnly: true,
                      onTap: () async {
                        DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2024),
                          lastDate: DateTime(2025),
                        );
                        String formatDate = DateFormat.yMd().format(
                          pickedDate!,
                        );
                        setState(
                          () {
                            dateController.text = formatDate;
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Container(
                margin: const EdgeInsets.only(bottom: 20),
                height: 50,
                width: 300,
                child: ElevatedButton(
                  style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromRGBO(1, 177, 167, 2),
                    ),
                    shape: MaterialStatePropertyAll(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                    ),
                  ),
                  onPressed: () {
                    doEdit ? addTask(doEdit, taskModelObj) : addTask(doEdit);
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Submit',
                    style: GoogleFonts.inter(
                      textStyle: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  List cardColor = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
  ];

  List<TaskModel> tasks = [
    TaskModel(
      title: 'Buy groceries',
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      date: '2021-12-31',
    ),
    TaskModel(
      title: 'Clean the house',
      description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      date: '2022-01-07',
    ),
  ];
  void addTask(bool doedit, [TaskModel? taskModelObj]) {
    String enteredTitle = titleController.text.trim();
    String enteredDescription = descriptionController.text.trim();
    String enteredDate = dateController.text.trim();

    if (enteredTitle.isNotEmpty &&
        enteredDescription.isNotEmpty &&
        enteredDate.isNotEmpty) {
      if (!doedit) {
        setState(() {
          tasks.add(
            TaskModel(
              title: enteredTitle,
              description: enteredDescription,
              date: enteredDate,
            ),
          );

          titleController.clear();
          descriptionController.clear();
          dateController.clear();
        });
      } else {
        setState(() {
          taskModelObj!.title = enteredTitle;
          taskModelObj.description = enteredDescription;
          taskModelObj.date = enteredDate;
        });
      }
    }
    // Clear text controllers
    titleController.clear();
    descriptionController.clear();
    dateController.clear();
  }

  ///REMOVE NOTES
  void removeTasks(TaskModel taskModelObj) {
    setState(() {
      tasks.remove(taskModelObj);
    });
  }

  void editTask(TaskModel taskModelObj) {
    titleController.text = taskModelObj.title;
    descriptionController.text = taskModelObj.description;
    dateController.text = taskModelObj.date;
    showBottomSheet(true, taskModelObj);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: nightMode ? Colors.black : Colors.white,
      appBar: AppBar(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(
              20,
            ),
          ),
        ),
        backgroundColor: const Color.fromRGBO(
          2,
          167,
          177,
          1,
        ),
        centerTitle: true,
        title: const Text(
          'To-Do List',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 26,
          ),
        ),
        leading: IconButton(
          tooltip: 'Switch to Night Mode',
          icon: Icon(
            size: 25,
            Icons.mode_night,
            color: nightMode ? Colors.white : Colors.black,
          ),
          onPressed: () {
            setState(() {
              if (!nightMode && !shadoColor) {
                nightMode = true;
                shadoColor = true;
              } else {
                nightMode = false;
                shadoColor = false;
              }
            });
          },
        ),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.logout,
              size: 25,
            ),
            color: Colors.white,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => LoginPage(),
                ),
              );
            },
            tooltip: 'Logout',
          ),
          const SizedBox(
            width: 15,
          ),
        ],
      ),
      body: Container(
        child: ListView.builder(
          itemCount: tasks.length,
          itemBuilder: (context, index) => Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(
              bottom: 10,
              top: 20,
              left: 15,
              right: 15,
            ),
            decoration: BoxDecoration(
              color: cardColor[index % cardColor.length],
              borderRadius: BorderRadius.circular(25),
              boxShadow: [
                BoxShadow(
                  color: shadoColor ? Colors.white : Colors.black,
                  offset: Offset(2, -1),
                  blurRadius: 8,
                ),
              ],
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      width: 80,
                      height: 80,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: const Icon(Icons.image),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            tasks[index].title,
                            style: GoogleFonts.quicksand(
                              textStyle: const TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            tasks[index].description,
                            style: GoogleFonts.quicksand(
                              textStyle: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 15,
                    ),
                    Text(
                      tasks[index].date,
                      style: GoogleFonts.quicksand(
                        textStyle: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15,
                        ),
                      ),
                    ),
                    const Spacer(),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            editTask(tasks[index]);
                          },
                          child: const Icon(
                            Icons.edit_outlined,
                            color: Color.fromRGBO(
                              2,
                              167,
                              177,
                              1,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        IconButton(
                          tooltip: 'Delete',
                          onPressed: () {
                            removeTasks(tasks[index]);
                          },
                          icon: const Icon(
                            Icons.delete_outline_rounded,
                            color: Color.fromRGBO(
                              2,
                              167,
                              177,
                              1,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        onPressed: () {
          showBottomSheet(false);
        },
        child: const Icon(
          Icons.add,
        ),
      ),
    );
  }
}
