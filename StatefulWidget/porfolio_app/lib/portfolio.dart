import 'package:flutter/material.dart';

class Portfolio extends StatefulWidget {
  const Portfolio({super.key});

  @override
  State<Portfolio> createState() => _PortfolioState();
}

class _PortfolioState extends State<Portfolio> {
  int _counter = -1;
  void _incrementCounter() => setState(() => ++_counter);

  @override
  Widget build(BuildContext context) {
    bool isFavoritePressed = false;

    List<String> images = [
      'https://media.licdn.com/dms/image/C4E0BAQH0NLW7tbvv5A/company-logo_200_200/0/1630634408955?e=2147483647&v=beta&t=49Ao02dltsRRNBISw5HnZ4dOxAD9RrxLooeei4Gnank',
      'https://cdn1.iconfinder.com/data/icons/google-s-logo/150/Google_Icons-09-512.png'
    ];
    return Scaffold(
      appBar: AppBar(
        leading: DrawerButtonIcon(),
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                isFavoritePressed = !isFavoritePressed;
              });
            },
            icon: Icon(
              Icons.favorite_border_outlined,
              size: 35,
              color: isFavoritePressed ? Colors.red : Colors.black,
            ),
          ),
          const SizedBox(width: 10),
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.account_circle,
              size: 35,
            ),
          ),
          const SizedBox(width: 10),
        ],
        backgroundColor: const Color.fromARGB(255, 104, 255, 164),
        title: const Row(
          children: [
            Text(
              'My',
              style: TextStyle(
                color: Colors.orange,
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(width: 10),
            Text(
              'Portfolio',
              style: TextStyle(
                color: Colors.purple,
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      backgroundColor: const Color.fromARGB(255, 255, 205, 239),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _incrementCounter();
        },
        child: const Text(
          'Next',
          style: TextStyle(fontSize: 18),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 20, left: 60),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const Text(
                    'Name : ',
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  (_counter >= 0)
                      ? const Text(
                          'Rugved Belkundar',
                          style: TextStyle(fontSize: 25),
                        )
                      : const SizedBox(),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              (_counter >= 1)
                  ? Image.asset(
                      'assets/pic.jpg',
                      height: 140,
                      width: 250,
                    )
                  : const SizedBox(),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  (_counter >= 2)
                      ? const Text(
                          'Collage : ',
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        )
                      : const SizedBox(),
                  const SizedBox(
                    width: 5,
                  ),
                  (_counter >= 3)
                      ? const Text(
                          "TSSM'S BSCOER Pune",
                          style: TextStyle(fontSize: 25),
                        )
                      : const SizedBox(),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              (_counter >= 4)
                  ? Image.network(
                      images[0],
                      height: 120,
                      width: 270,
                    )
                  : const SizedBox(),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  (_counter >= 5)
                      ? const Text(
                          'Dream Company : ',
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        )
                      : const SizedBox(),
                  const SizedBox(
                    width: 5,
                  ),
                  (_counter >= 6)
                      ? const Text(
                          "Google",
                          style: TextStyle(fontSize: 25),
                        )
                      : const SizedBox(),
                ],
              ),
              (_counter >= 7)
                  ? Image.network(
                      images[1],
                      height: 150,
                      width: 250,
                    )
                  : const SizedBox(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (_counter >= 8)
                      ? ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _counter = -1;
                            });
                          },
                          child: Text('Reset'),
                        )
                      : const SizedBox(),
                  const SizedBox(width: 20),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
