import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({Key? key}) : super(key: key);

  @override
  State<Assignment3> createState() => _Assignment3State();
}

class _Assignment3State extends State<Assignment3> {
  int selectedIndex = 0;

  final List<String> imageList = [
    'https://nonprod-media.webdunia.com/public_html/_media/hi/img/article/2020-12/07/full/1607311219-0037.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxGuk4fvy2UZxTMZbmC2-37JcGnZwZDLMg8w&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGOrExj44piQQd2MeRNTDsFCnGu2T92LuM2Q&usqp=CAU',
  ];

  void showNextImage() {
    setState(() {
      if (selectedIndex < imageList.length - 1) {
        selectedIndex = selectedIndex + 1;
      } else {
        selectedIndex = 0; // Reset to the first image
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Display Images'),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
              imageList[selectedIndex],
              width: 300,
              height: 300,
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: showNextImage,
              child: const Text('Next'),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                setState(
                  () {
                    selectedIndex = 0;
                  },
                );
              },
              child: const Text('Reset'),
            ),
          ],
        ),
      ),
    );
  }
}
