import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _count = 0;
  List<String> names = [
    "Rohit",
    "Shivv",
    "Mayur",
    "Vaibhav",
    "Manoj",
    "Prajwal",
    "Rugved",
    "Darshan",
    "Athrav",
    "Sangram",
    "Vivek",
    "Abhinav",
    "Saurabh",
    "Prathamesh"
  ];
  List<String> names1 = List.empty(growable: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Snapchat",
            style: TextStyle(fontWeight: FontWeight.bold),
            selectionColor: Colors.amber),
        centerTitle: true,
        backgroundColor: Colors.amber,
      ),
      body: ListView.builder(
        itemCount: names1.length,
        itemBuilder: (context, index) {
          return Container(
            height: 50,
            width: double.infinity,
            child: Row(
              children: [
                Icon(Icons.person),
                SizedBox(width: 10),
                Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${names1[index]}",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text("Received" "  " "  " "00h" "  " "500")
                    ]),
                Icon(Icons.fire_extinguisher)
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(
            () {
              if (_count <= names.length) {
                _count++;
                names1.add(names[_count]);
              } else {
                _count = 0;
              }
            },
          );
        },
        child: const Icon(Icons.skip_next),
      ),
    );
  }
}
