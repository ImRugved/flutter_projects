import 'package:flutter/material.dart';

class DisplayImageDynamic extends StatefulWidget {
  const DisplayImageDynamic({Key? key}) : super(key: key);

  @override
  State<DisplayImageDynamic> createState() => _DisplayImageDynamicState();
}

class _DisplayImageDynamicState extends State<DisplayImageDynamic> {
  List<String> imagesList = [];
  int counter = 0;
  // A list of initial images to be added when the button is pressed
  List<String> imageList = [
    'https://dnm.nflximg.net/api/v6/2DuQlx0fM4wd1nzqm5BFBi6ILa8/AAAAQeIeKt7LlqIJPKrT4aQijclj7K43xRSU3dQXNESNdNbnnJbT6LLWVRT9srUUbHbOo-iOH-8v3o16pUDMQ6tCgNGlkvfwvDOprROIZpQ2rgHtop9rHvbYlvzavMmUSGBCXjynJ80dn4nqZzZmzIUJMQpS.jpg?r=943',
    'https://www.tallengestore.com/cdn/shop/products/PeakyBlinders-NetflixTVShow-ArtPoster_125897c4-6348-41e8-b195-d203700ebcca.jpg?v=1619864555',
    'https://dnm.nflximg.net/api/v6/2DuQlx0fM4wd1nzqm5BFBi6ILa8/AAAAQeIeKt7LlqIJPKrT4aQijclj7K43xRSU3dQXNESNdNbnnJbT6LLWVRT9srUUbHbOo-iOH-8v3o16pUDMQ6tCgNGlkvfwvDOprROIZpQ2rgHtop9rHvbYlvzavMmUSGBCXjynJ80dn4nqZzZmzIUJMQpS.jpg?r=943'
  ];
  void addImageCounter() {
    setState(
      () {
        if (counter < imageList.length) {
          imagesList.add(imageList[counter]);
          counter++;
        }
      },
    );
    print(counter);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: Text('Listview Builder'),
      ),
      body: ListView.builder(
        itemCount: imagesList.length,
        itemBuilder: (context, index) {
          return Container(
            height: 500,
            width: 100,
            margin: EdgeInsets.all(10),
            child: Image.network(
              imagesList[index],
              fit: BoxFit.cover,
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          addImageCounter();
        },
        child: Icon(Icons.next_plan),
      ),
    );
  }
}
