import 'package:flutter/material.dart';
import 'package:listview/displayimage.dart';
import 'package:listview/displayimgDynamic.dart';
import 'package:listview/displaynames.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //  home: DisplayImage(),
      // home: DisplayImageDynamic(),
      home: MyHomePage(title: 'ListView Demo'),
    );
  }
}
