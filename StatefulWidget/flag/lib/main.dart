import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: IndianFlag(),
    );
  }
}

class IndianFlag extends StatefulWidget {
  @override
  State<IndianFlag> createState() => _IndianFlagState();
}

class _IndianFlagState extends State<IndianFlag> {
  int _counter = -1;
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    String ashoka =
        'https://cdn.pixabay.com/photo/2023/04/06/16/29/ashoka-chakra-7904695_960_720.png';
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 234, 255, 0),
        centerTitle: true,
        title: const Row(
          children: [
            SizedBox(
              width: 10,
            ),
            Text(
              'HAPPY',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 255, 89, 0),
                shadows: [
                  Shadow(
                    color: Colors.black,
                    blurRadius: 3,
                    offset: Offset.zero,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'REPUBLIC',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Colors.white,
                decorationColor: Colors.black,
                shadows: [
                  Shadow(
                    color: Colors.black,
                    blurRadius: 3,
                    offset: Offset.zero,
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'DAY',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Colors.green,
                shadows: [
                  Shadow(
                    color: Colors.black,
                    blurRadius: 3,
                    offset: Offset.zero,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _incrementCounter();
          });
        },
        child: const Icon(Icons.add),
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromARGB(255, 251, 171, 50),
              Color.fromARGB(255, 255, 255, 255),
              Color.fromARGB(255, 113, 255, 118),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        // margin: EdgeInsets.only(top: 50),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (_counter >= 0)
                ? Container(
                    height: 530,
                    width: 10,
                    decoration: const BoxDecoration(
                      color: Color.fromARGB(255, 59, 59, 59),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                    ),
                  )
                : const SizedBox(),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                (_counter >= 1)
                    ? Container(
                        margin: const EdgeInsets.only(top: 60),
                        height: 65,
                        width: 300,
                        decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 255, 132, 0),
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10.0),
                          ),
                        ),
                      )
                    : const SizedBox(),
                (_counter >= 2)
                    ? Container(
                        alignment: Alignment.center,
                        height: 65,
                        width: 300,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                        ),
                        child: (_counter >= 3)
                            ? Image.network(ashoka)
                            : const SizedBox(),
                      )
                    : const SizedBox(),
                (_counter >= 4)
                    ? Container(
                        height: 65,
                        width: 300,
                        decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 43, 138, 46),
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(10.0),
                          ),
                        ),
                      )
                    : const SizedBox(),
                const SizedBox(
                  height: 50,
                ),
                (_counter >= 5)
                    ? const Text(
                        'भारतीय ',
                        style: TextStyle(fontSize: 60),
                      )
                    : const SizedBox(),
                (_counter >= 6)
                    ? const Text(
                        'गणराज्य ',
                        style: TextStyle(fontSize: 60),
                      )
                    : const SizedBox(),
                (_counter >= 7)
                    ? const Text(
                        'दिवस ',
                        style: TextStyle(fontSize: 60),
                      )
                    : const SizedBox(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
