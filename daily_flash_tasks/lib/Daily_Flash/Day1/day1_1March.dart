/*
1. Create an AppBar, give an Icon at the start of the appbar, give a title
in the middle, and at the end add an Icon.
2. Create an AppBar give a color of your choice to the AppBar and then
add an icon at the start of the AppBar and 3 icons at the end of the
AppBar.
3. Create a Screen that will display an AppBar. Add a title in the AppBar
the app bar must have a round rectangular border at the bottom.
4. Create a Screen that will display the Container in the Center of the
Screen,
with size(width: 300, height: 300). The container must have a blue
color and it must have a border which must be of color red.
5. Create a Screen, in the center of the Screen display a Container with
rounded corners, give a specific color to the Container, the container
must have a shadow of color red.
*/
import 'package:flutter/material.dart';

class Day1 extends StatelessWidget {
  const Day1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: const Text(
          'DailyFlash Day2',
        ),
        centerTitle: true,
        leading: const Icon(
          Icons.verified_user_sharp,
        ),
        actions: const [
          Icon(Icons.comment),
          Icon(Icons.edit),
          Icon(Icons.delete),
        ],
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(
              10,
            ),
          ),
        ),
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
            color: Colors.blue,
            border: Border.all(
              color: Colors.yellow,
              width: 10,
            ),
            borderRadius: BorderRadius.circular(
              20,
            ),
            boxShadow: const [
              BoxShadow(
                color: Colors.red,
                offset: Offset(10, 10),
                blurRadius: 15,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
