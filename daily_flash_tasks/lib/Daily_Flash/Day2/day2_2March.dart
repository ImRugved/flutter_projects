import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Day2 extends StatefulWidget {
  const Day2({super.key});

  @override
  State<Day2> createState() => _Day2State();
}

class _Day2State extends State<Day2> {
  bool containerColor = false;
  bool text = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text('Day 2'),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.only(
          top: 10,
          bottom: 30,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              /*
              1.Create a screen that displays the container in the center having size (height:200, width: 200). The Container must have border with rounded edges. Theborder must be of the color red. Display a Text in the center of the container.
              */
              Container(
                height: 200,
                width: 200,
                decoration: BoxDecoration(
                  color: Colors.yellow,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(
                      20,
                    ),
                  ),
                  border: Border.all(
                    color: Colors.red,
                    width: 5,
                  ),
                ),
                child: const Center(
                  child: Text(
                    'Rugved',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  /*
              2. In the screen add a container of size( width 100, height: 100) that must only have a left border of width 5 and color as per your choice. Give padding to the container and display a text in the Container.
              */
                  Container(
                    padding: const EdgeInsets.all(20),
                    height: 100,
                    width: 100,
                    decoration: const BoxDecoration(
                      color: Colors.pink,
                    ),
                    child: const Text(
                      'Rugved',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  /*
              3. In the screen add a container of size( width 100, height: 100) . The container must have a border as displayed in the below image. Give color to the container and border as per your choice.
              */
                  Container(
                    padding: const EdgeInsets.all(20),
                    height: 100,
                    width: 130,
                    decoration: BoxDecoration(
                      color: Colors.purple[300],
                      borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(
                          20,
                        ),
                      ),
                      border: Border.all(
                        color: Colors.orange,
                        width: 3,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              /*
             4. Create a container that will have a border. The top right and bottom left corners of the border must be rounded. Now display the Text in the Container and give appropriate padding to the container.
              */
              Container(
                padding: const EdgeInsets.all(20),
                height: 80,
                width: 170,
                decoration: BoxDecoration(
                  color: Colors.pink[100],
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(
                      30,
                    ),
                    bottomRight: Radius.circular(
                      30,
                    ),
                  ),
                  border: Border.all(
                    color: Colors.pinkAccent,
                    width: 3,
                  ),
                ),
                child: const Text('Rugved Belkundkar'),
              ),
              const SizedBox(
                height: 10,
              ),
              /*
5. Add a container with the color red and display the text "Click me!" in the center of the container. On tapping the container, the text must change to “Container Tapped” and the color of the container must change to blue.
    */
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (containerColor == false && text == false) {
                      containerColor = true;
                      text = true;
                    } else {
                      containerColor = false;
                      text = false;
                    }
                  });
                },
                child: Container(
                  height: 150,
                  width: 150,
                  color: containerColor ? Colors.blue : Colors.red,
                  child: Center(
                    child: text
                        ? const Text('Container Tapped')
                        : const Text('Click me!'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
