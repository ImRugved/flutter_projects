import 'package:daily_flash_tasks/Daily_Flash/Day1/day1_1March.dart';
import 'package:daily_flash_tasks/Daily_Flash/Day2/day2_2March.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Day2(),
    );
  }
}
