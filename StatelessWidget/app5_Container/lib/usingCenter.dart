import 'package:flutter/material.dart';

class UsingCenter extends StatelessWidget {
  const UsingCenter({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[100],
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text('Container'),
      ),
      body: Center(
        heightFactor: 1.5,
        widthFactor: 2.5,
        child: Container(
          height: 100,
          width: 100,
          color: Colors.red,
        ),
      ),
    );
  }
}
