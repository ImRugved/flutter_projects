import 'package:flutter/material.dart';

class UsingContainer extends StatelessWidget {
  const UsingContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.yellow[100],
          appBar: AppBar(
            backgroundColor: Colors.blue,
            title: const Text('Container'),
          ),
          body: Container(
            height: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.purple,
                ),
                const SizedBox(
                  width: 10,
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.black,
                ),
              ],
            ),
          )),
    );
  }
}
