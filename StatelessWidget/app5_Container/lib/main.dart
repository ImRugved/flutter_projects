import 'package:app1/home.dart';
import 'package:app1/usingCenter.dart';
import 'package:app1/usingContainer.dart';
import 'package:app1/usingSizedBox.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: UsingSizedBox(),
    );
  }
}
