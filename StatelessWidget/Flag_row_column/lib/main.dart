import 'package:flutter/material.dart';
import 'package:row_column/Flag.dart';

import 'package:row_column/UsingColumn.dart';
import 'package:row_column/UsingRow.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Flag(),
    );
  }
}
