import 'package:flutter/material.dart';
import 'package:inherited_ex/main.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    ShareData shareDataObj =
        context.dependOnInheritedWidgetOfExactType<ShareData>()!;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Inheritaed examplse'),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '${shareDataObj.data}',
          ),
          const SizedBox(
            height: 20,
          ),
          const AccessDataWidget()
        ],
      ),
    );
  }
}

class AccessDataWidget extends StatelessWidget {
  const AccessDataWidget({super.key});

  @override
  Widget build(BuildContext context) {
    // ShareData shareDataObj =
    //     context.dependOnInheritedWidgetOfExactType<ShareData>()!;
    ShareData shareDataObj = ShareData.of(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('${shareDataObj.data}'),
        const SizedBox(
          width: 10,
        ),
        const AccessDataChild()
      ],
    );
  }
}

class AccessDataChild extends StatelessWidget {
  const AccessDataChild({super.key});

  @override
  Widget build(BuildContext context) {
    ShareData shareDataObj =
        context.dependOnInheritedWidgetOfExactType<ShareData>()!;
    return Text('${shareDataObj.data}');
  }
}
