import 'package:flutter/material.dart';
import 'package:inherited_ex/ex1.dart';

void main() => runApp(const MyApp());

class ShareData extends InheritedWidget {
  final int data;
  const ShareData({
    super.key,
    required this.data,
    required super.child,
  });
  static ShareData of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ShareData>()!;
  }

  @override
  bool updateShouldNotify(ShareData oldWidget) {
    return data != oldWidget.data;
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const ShareData(
      data: 50,
      child: MaterialApp(
        home: HomePage(),
      ),
    );
  }
}
