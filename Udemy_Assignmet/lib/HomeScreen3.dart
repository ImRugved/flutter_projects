import 'package:flutter/material.dart';

class HomeScreen3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Images',
      home: DisplayImage(),
    );
  }
}

class DisplayImage extends StatefulWidget {
  const DisplayImage({super.key});

  @override
  _DisplayImageState createState() => _DisplayImageState();
}

class _DisplayImageState extends State<DisplayImage> {
  int currentIndex = 0;

  List<String> images = [
    'images/dart.png',
    'images/flutter.png',
    'images/python.png',
  ];

  void changeImage() {
    setState(() {
      currentIndex = (currentIndex + 1) % images.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text('Image Show'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 200,
              child: Image.asset(
                images[currentIndex],
                //fit: BoxFit.cover,
              ),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: changeImage,
              child: const Text('Change Image'),
            ),
          ],
        ),
      ),
    );
  }
}
