import 'package:flutter/material.dart';

class HomeScreen5 extends StatefulWidget {
  const HomeScreen5({super.key});
  @override
  _HomeScreen5State createState() => _HomeScreen5State();
}

class _HomeScreen5State extends State<HomeScreen5> {
  List<Color> containerColors = [Colors.white, Colors.white, Colors.white];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Title'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildContainer(0),
            buildContainer(1),
            buildContainer(2),
          ],
        ),
      ),
    );
  }

  Widget buildContainer(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          // Reset all container colors to white
          containerColors = List.filled(3, Colors.white);
          // Set the tapped container to red
          containerColors[index] = Colors.red;
        });
      },
      child: Container(
        width: 100,
        height: 100,
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          color: containerColors[index],
        ),
      ),
    );
  }
}
