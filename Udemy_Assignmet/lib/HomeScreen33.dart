import 'package:flutter/material.dart';

class HomeScreen33 extends StatelessWidget {
  const HomeScreen33({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Images',
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 20),
                Image.asset(
                  'images/dart.png',
                  //width: 500,
                  height: 150,
                  fit: BoxFit.cover,
                ),
                const SizedBox(height: 20),
                Image.asset(
                  'images/flutter.png',
                  //width: 200,
                  height: 150,
                  fit: BoxFit.cover,
                ),
                const SizedBox(height: 20),
                Image.asset(
                  'images/python.png',
                  //width: 200,
                  height: 150,
                  fit: BoxFit.cover,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
