import 'package:flutter/material.dart';

class HomeScreen4 extends StatelessWidget {
  const HomeScreen4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Images',
      color: Colors.black,
      home: Scaffold(
        backgroundColor: Colors.amber,
        appBar: AppBar(),
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //const SizedBox(width: 20),
              Image.asset(
                'images/dart.png',
                width: 115,
                height: 60,
              ),
              const SizedBox(width: 20),
              Image.asset(
                'images/flutter.png',
                width: 115,
                height: 80,
              ),
              const SizedBox(width: 20),
              Image.asset(
                'images/python.png',
                width: 115,
                height: 60,
              ),
            ],
          ),
        ),
      ),
    );
  }
}