import 'package:flutter/material.dart';

class HomeScreen2 extends StatelessWidget {
  const HomeScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Flutter App")),
      backgroundColor: Colors.yellow,
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 251, 0, 255), // First color
                Color.fromARGB(255, 55, 255, 0), // Second color
              ],
              stops: [0.5, 0.5], // it divides the containe half in two part
            ),
          ),
          child: const Center(
              child: Text(
            'Rugved',
            style: TextStyle(fontSize: 30),
          )),
        ),
      ),
    );
  }
}
