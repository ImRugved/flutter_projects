import 'package:flutter/material.dart';

class HomeScreen1 extends StatelessWidget {
  const HomeScreen1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("title"),
      ),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              color: Colors.orange, borderRadius: BorderRadius.circular(20)),
          height: 200,
          width: 200,
          child: const Center(
              child: Text(
            'Rugved',
            style: TextStyle(fontSize: 30),
          )),
        ),
      ),
    );
  }
}
