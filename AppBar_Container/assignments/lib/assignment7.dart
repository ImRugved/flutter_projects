import 'package:flutter/material.dart';

class Assignment7 extends StatelessWidget {
  const Assignment7({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('AppBar'),
        ),
        body: Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Image.network(
                    width: 150,
                    height: 300,
                    'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg'),
                const SizedBox(
                  width: 10,
                ),
                Image.network(
                    width: 150,
                    height: 300,
                    'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg'),
                const SizedBox(
                  width: 10,
                ),
                Image.network(
                    width: 150,
                    height: 300,
                    'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg'),
                const SizedBox(
                  width: 10,
                ),
                Image.network(
                    width: 150,
                    height: 300,
                    'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg'),
                const SizedBox(
                  width: 10,
                ),
                Image.network(
                    width: 150,
                    height: 300,
                    'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg'),
                const SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
        ),
      );
  }
}