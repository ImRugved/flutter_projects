import 'package:flutter/material.dart';

class Assignment5 extends StatelessWidget {
  const Assignment5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('AppBar'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              height: 200,
              width: 200,
              'assets/images/dart.png',
            ),
            const SizedBox(
              height: 10,
            ),
            Image.asset(
              height: 200,
              width: 200,
              'assets/images/flogo.png',
            ),
          ],
        ),
      ),
    );
  }
}
