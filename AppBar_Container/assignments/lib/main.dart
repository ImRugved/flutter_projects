import 'package:assignment5/assignment1.dart';
import 'package:assignment5/assignment10.dart';
import 'package:assignment5/assignment2.dart';
import 'package:assignment5/assignment3.dart';
import 'package:assignment5/assignment4.dart';
import 'package:assignment5/assignment6.dart';
import 'package:assignment5/assignment7.dart';
import 'package:assignment5/assignment8.dart';
import 'package:assignment5/assignment9.dart';
import 'package:assignment5/padding_margin.dart';
import 'package:flutter/material.dart';
import 'package:assignment5/assignment5.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      //home: Assignment10(),
      home: PaddingAssignment(),
    );
  }
}
