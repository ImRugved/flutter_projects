import 'package:flutter/material.dart';

class Assignment8 extends StatelessWidget {
  const Assignment8({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('appBar'),
        ),
        body: Center(
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.red), color: Colors.yellow),
            height: 200,
            width: 200,
          ),
        ),
      );
  }
}