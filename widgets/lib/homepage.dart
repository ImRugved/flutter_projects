import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<String> courses = [
    'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Dart_logo.png/768px-Dart_logo.png',
    'https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Flutter_logo.svg/640px-Flutter_logo.svg.png',
    'https://w7.pngwing.com/pngs/640/199/png-transparent-javascript-logo-html-javascript-logo-angle-text-rectangle-thumbnail.png',
    'https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Python_logo_55.svg/800px-Python_logo_55.svg.png',
    'https://w7.pngwing.com/pngs/578/816/png-transparent-java-class-file-java-platform-standard-edition-java-development-kit-java-runtime-environment-coffee-jar-text-class-orange-thumbnail.png'
  ];

  int counter = 0;
  void incrCounter() {
    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[200],
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.favorite_border_outlined),
          ),
          const SizedBox(
            width: 10,
          )
        ],
        backgroundColor: Colors.black,
        title: const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "Welcome",
              style: TextStyle(fontSize: 25, color: Colors.purple),
            ),
            Text(
              "To",
              style: TextStyle(fontSize: 25, color: Colors.white),
            ),
            Text(
              "Flutter",
              style: TextStyle(fontSize: 25, color: Colors.orange),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Courses',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image.network(
                        height: 300,
                        width: 300,
                        courses[0],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 50,
                        width: 200,
                        child: ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            'Dart',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Column(
                    children: [
                      Image.network(
                        height: 300,
                        width: 300,
                        courses[1],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 50,
                        width: 200,
                        child: ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            'Flutter',
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ListView(
              shrinkWrap: true,
              children: [
                (counter > 0)
                    ? Text(
                        'Dart :',
                        style: TextStyle(fontSize: 25),
                      )
                    : Container(),
                (counter > 1)
                    ? Text(
                        "Dart is an open-source, general-purpose programming language developed by Google in 2011. It's used to create web and mobile apps, as well as server and desktop applications.Dart is object-oriented, client-optimized, and has a C-style syntax. It's widely used for developing Android and iOS apps, IoT (Internet of Things), and web applications using the Flutter Framework")
                    : Container(),
                Text(
                  'Flutter :',
                  style: TextStyle(fontSize: 25),
                ),
                Text(
                    "Flutter is a free, open-source software development kit (SDK) for building mobile applications. It was released in May 2017 by Google It uses a single codebase, which means that developers can use one programming language and codebase to create two different apps, one for iOS and one for Android. Flutter uses a reactive programming language called Dart, which makes development faster and easier than traditional methods"),
              ],
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        items: const [
          BottomNavigationBarItem(
            backgroundColor: Colors.purple,
            icon: Icon(
              Icons.home,
              color: Colors.red,
              size: 40,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.search,
              size: 40,
            ),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.add,
              size: 40,
            ),
            label: 'Favorites',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.favorite,
              size: 35,
            ),
            label: 'Cart',
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.person,
              size: 40,
            ),
            label: 'Profile',
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
      ),
    );
  }
}
