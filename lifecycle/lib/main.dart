import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TextFieldDemo(),
    );
  }
}

class TextFieldDemo extends StatefulWidget {
  const TextFieldDemo({super.key});

  @override
  State createState() => _TextFieldDemo();
}

class _TextFieldDemo extends State<TextFieldDemo> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _numberController = TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _numberFocusNode = FocusNode();

  final List<String> _enteredNameValue = [];
  final List<String> _enteredEmailValue = [];
  final List<String> _enteredNumberValue = [];

  Map<String, dynamic> data = {
    'Name': [],
    "Email": [],
    "Number": [],
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('To-do list'),
        backgroundColor: Color.fromRGBO(1, 177, 167, 2),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(height: 30),
            SizedBox(
              width: 400,
              child: TextField(
                controller: _nameController,
                //focusNode: _nameFocusNode,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Enter Your Name",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                ),
                cursorColor: Colors.black,
                textInputAction: TextInputAction.done,
                onChanged: (value) {
                  print('Value is: $value');
                },
                onSubmitted: (value) {
                  print('sumtted val is :$value');
                },
              ),
            ),
            const SizedBox(height: 20),
            SizedBox(
              width: 400,
              child: TextField(
                controller: _emailController,
                focusNode: _emailFocusNode,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Enter Your Email",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                ),
                cursorColor: Colors.black,
                textInputAction: TextInputAction.done,
                onChanged: (value) {
                  print('Value is: $value');
                },
                onSubmitted: (value) {
                  print('sumtted val is :$value');
                },
              ),
            ),
            const SizedBox(height: 20),
            SizedBox(
              width: 400,
              child: TextField(
                controller: _numberController,
                focusNode: _numberFocusNode,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Enter Your Number",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(color: Colors.blue),
                  ),
                ),
                cursorColor: Colors.black,
                textInputAction: TextInputAction.done,
                onChanged: (value) {
                  print('Value is: $value');
                },
                onSubmitted: (value) {
                  print('sumtted val is :$value');
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(10),
                child: ListView.builder(
                  itemCount: _enteredNameValue.length,
                  itemBuilder: (context, index) {
                    return Expanded(
                      child: Card(
                        child: ListTile(
                          leading: Column(
                            children: [
                              CircleAvatar(
                                child: Image.network('src'),
                              ),
                              Text(_enteredNumberValue[index]),
                            ],
                          ),
                          title: Text(
                            _enteredNameValue[index],
                          ),
                          subtitle: Text(
                            _enteredEmailValue[index],
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                icon: const Icon(Icons.edit),
                                onPressed: () {
                                  // Implement edit functionality (modify existing task)
                                  // ...
                                },
                              ),
                              IconButton(
                                icon: const Icon(Icons.delete),
                                onPressed: () {
                                  setState(() {
                                    // Remove task from lists and update UI
                                    _enteredNameValue.removeAt(index);
                                    _enteredEmailValue.removeAt(index);
                                    _enteredNumberValue.removeAt(index);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _addNameToList();
          _addEmailToList();
          _addNumberToList();
        },
        child: Icon(Icons.add),
      ),
    );
  }

  void _addNameToList() {
    setState(
      () {
        String enteredName = _nameController.text.trim();
        if (enteredName.isNotEmpty) {
          _enteredNameValue.add(enteredName);
          _nameController.clear();
        }
      },
    );
  }

  void _addEmailToList() {
    setState(
      () {
        String enteredEmail = _emailController.text.trim();
        if (enteredEmail.isNotEmpty) {
          _enteredEmailValue.add(enteredEmail);
          _emailController.clear();
        }
      },
    );
  }

  void _addNumberToList() {
    setState(
      () {
        String enteredNumber = _numberController.text;
        if (enteredNumber.isNotEmpty) {
          _enteredNumberValue.add(enteredNumber);
          _numberController.clear();
        }
      },
    );
  }
}
