import 'dart:async';
import 'package:flutter/material.dart';
import 'package:quiz_app/pages/homepage.dart';
import 'package:quiz_app/pages/profile.dart';
import 'package:quiz_app/quiz_c2w.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerQuiz extends StatefulWidget {
  ShimmerQuiz({super.key});
  @override
  State createState() => _ShimmerQuizState();
}

class _ShimmerQuizState extends State<ShimmerQuiz> {
  int navBarIndex = 0;
  int queCounter = 1;
  bool showShimmer = true;

  List<String> quesList = [
    'What is Flutter?',
    'When Flutter was introduced ?',
    'Which Programming language flutter use ?',
    'How many types of widgets are there in Flutter?',
    'Which widget is used to add space between two widgets',
    'What are the features of Flutter?',
    'Which of the following is used to load images from the flutter project’s assets?',
    'What does SDK stands for?',
    'A widget that allows us to see the change on the screen is called... ',
    'Which of the following are the advantages of Flutter?'
  ];

  void incrementQuestion() {
    setState(() {
      if (queCounter < quesList.length) {
        queCounter++;
      }
    });
  }

  void decrementQuestion() {
    setState(() {
      if (queCounter > 1) {
        queCounter--;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 2), () {
      setState(() {
        showShimmer = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[100],
      appBar: AppBar(
        backgroundColor: Colors.pink,
        centerTitle: true,
        title: const Text('Tech Quiz'),
      ),
      body: showShimmer
          ? Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                margin: const EdgeInsets.only(top: 40),
                height: double.infinity,
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Text(
                          'Questions : ',
                          style: TextStyle(
                            fontSize: 40,
                          ),
                        ),
                        Text(
                          '$queCounter/${quesList.length}',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    if (queCounter <= quesList.length)
                      Text(
                        '${quesList[queCounter - 1]}',
                        style: TextStyle(
                          fontSize: 40,
                        ),
                      ),
                    const SizedBox(height: 30),
                    SizedBox(
                      width: 300,
                      child: ElevatedButton(
                        onPressed: () {},
                        child: const Text(
                          'Database',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                      width: 300,
                      child: ElevatedButton(
                        onPressed: () {},
                        child: const Text(
                          'Framework',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                      width: 300,
                      child: ElevatedButton(
                        onPressed: () {},
                        child: const Text(
                          'Programming language',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                      width: 300,
                      child: ElevatedButton(
                        onPressed: () {},
                        child: const Text(
                          'OS',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 90),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(Colors.green),
                          ),
                          onPressed: () {
                            incrementQuestion();
                          },
                          child: const Text(
                            'NEXT',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        const SizedBox(width: 50),
                        ElevatedButton(
                          style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(Colors.redAccent),
                          ),
                          onPressed: () {
                            decrementQuestion();
                          },
                          child: const Text(
                            'BACK',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          : Container(
              margin: const EdgeInsets.only(top: 40),
              height: double.infinity,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text('Questions : '),
                      Text('$queCounter/${quesList.length}'),
                    ],
                  ),
                  const SizedBox(height: 10),
                  if (queCounter <= quesList.length)
                    Text('${quesList[queCounter - 1]}'),
                  const SizedBox(height: 30),
                  SizedBox(
                    width: 300,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: const Text('Database'),
                    ),
                  ),
                  const SizedBox(height: 20),
                  SizedBox(
                    width: 300,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: const Text('Framework'),
                    ),
                  ),
                  const SizedBox(height: 20),
                  SizedBox(
                    width: 300,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: const Text('Programming language'),
                    ),
                  ),
                  const SizedBox(height: 20),
                  SizedBox(
                    width: 300,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: const Text('OS'),
                    ),
                  ),
                  const SizedBox(height: 90),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.green),
                        ),
                        onPressed: () {
                          incrementQuestion();
                        },
                        child: const Text(
                          'NEXT',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(width: 50),
                      ElevatedButton(
                        style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.redAccent),
                        ),
                        onPressed: () {
                          decrementQuestion();
                        },
                        child: const Text(
                          'BACK',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
      bottomNavigationBar: SalomonBottomBar(
        unselectedItemColor: Colors.black,
        margin: const EdgeInsets.all(15),
        backgroundColor: Colors.yellow[400],
        currentIndex: navBarIndex,
        onTap: (i) => setState(() => navBarIndex = i),
        items: [
          SalomonBottomBarItem(
            icon: IconButton(
              iconSize: 30,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage(),
                  ),
                );
              },
              icon: const Icon(Icons.home),
            ),
            title: const Text("Home"),
            selectedColor: Colors.blueAccent,
          ),

          /// Search
          SalomonBottomBarItem(
            icon: IconButton(
              iconSize: 30,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const QuizApp(),
                  ),
                );
              },
              icon: const Icon(Icons.quiz_rounded),
            ),
            title: const Text(
              "Quiz",
            ),
            selectedColor: Colors.purple,
          ),

          /// Profile
          SalomonBottomBarItem(
            icon: IconButton(
              iconSize: 30,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Profile(),
                  ),
                );
              },
              icon: const Icon(Icons.person),
            ),
            title: const Text("Profile"),
            selectedColor: Colors.green,
          ),
        ],
      ),
    );
  }
}
