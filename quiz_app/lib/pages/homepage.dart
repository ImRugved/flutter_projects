import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';

import 'package:quiz_app/pages/profile.dart';
import 'package:quiz_app/pages/shimmerquiz.dart';
import 'package:quiz_app/quiz_c2w.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: const Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Welcome to the Home Page!',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w800,
              ),
            ),
            SizedBox(
              height: 60,
            ),
            Text(
              textAlign: TextAlign.center,
              'Click Below Button and Go to Quiz Page !',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(
              height: 250,
            ),
            Text(
              textAlign: TextAlign.center,
              ' Home page is under development...',
              style: TextStyle(fontSize: 34, color: Colors.black87),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ConvexAppBar(
        disableDefaultTabController: true,
        height: 60,
        backgroundColor: Colors.black,
        items: [
          TabItem(
              icon: IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ShimmerQuiz(),
                    ),
                  );
                },
                icon: Icon(
                  Icons.school_rounded,
                ),
              ),
              title: 'Learn'),
          TabItem(
            title: '   Quiz',
            icon: IconButton(
              iconSize: 25,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const QuizApp(),
                  ),
                );
              },
              icon: const Icon(
                Icons.quiz_rounded,
              ),
            ),
          ),
          TabItem(
            title: "Profile",
            icon: IconButton(
              focusColor: Colors.white,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Profile(),
                  ),
                );
              },
              icon: Icon(
                Icons.people,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
