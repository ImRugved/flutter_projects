import 'package:flutter/material.dart';

import 'package:quiz_app/pages/homepage.dart';
import 'package:quiz_app/pages/profile.dart';
import 'package:quiz_app/pages/shimmerquiz.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class SingleQuestionModel {
  final String? question;
  final List<String>? options;
  final int? answerIndex;

  const SingleQuestionModel({this.question, this.options, this.answerIndex});
}

class _QuizAppState extends State {
  int navBarIndex = 0;
  bool questionFlag = true;
  int questionIndex = 0;
  int correctAnswer = 0;
  int selectedAnswerIndex = -1;
  final allQuestions = [
    const SingleQuestionModel(
      question: " Tell Me What is Flutter?",
      options: ["Database", "Programming Language", "Framework", "OS"],
      answerIndex: 2,
    ),
    const SingleQuestionModel(
      question: "When Flutter was introduced ?",
      options: ["2011", "2017", "2018", "2013"],
      answerIndex: 1,
    ),
    const SingleQuestionModel(
      question: "Flutter is develoeped by ?",
      options: ["Microsoft", "Google", "Tesla", "Facebook"],
      answerIndex: 1,
    ),
    const SingleQuestionModel(
      question: "Which Programming language flutter use ?",
      options: ["Dart", "Java", "Python", "Go"],
      answerIndex: 0,
    ),
    const SingleQuestionModel(
      question: "How many types of widgets are there in Flutter?",
      options: ["1", "3", "4", "2"],
      answerIndex: 3,
    ),
    const SingleQuestionModel(
      question: "Which widget is used to add space between two widgets",
      options: ["Container", "Sized Box", "Text", "Color"],
      answerIndex: 1,
    ),
    const SingleQuestionModel(
      question: "What are the features of Flutter?",
      options: ["Open Surce", "Cross Platform", "Free", "All of the Above"],
      answerIndex: 3,
    ),
    const SingleQuestionModel(
      question: "Which of the following widget is used to add local images?",
      options: ["Image.network", "Image", "Image.asset", "Image.local"],
      answerIndex: 2,
    ),
    const SingleQuestionModel(
      question: "What does SDK stands for?",
      options: [
        "Solution Device Kit",
        "Software Development Kit",
        "Same Device Kit",
        "None of the Above"
      ],
      answerIndex: 1,
    ),
    const SingleQuestionModel(
      question:
          "A feature that allows us to see the instant change on the screen is called ?",
      options: ["Hot Restart", "Start", "Stop", "Hot Reload"],
      answerIndex: 3,
    ),
  ];
  MaterialStateProperty<Color?> checkAnswer(int buttonIndex) {
    if (selectedAnswerIndex != -1) {
      if (buttonIndex == allQuestions[questionIndex].answerIndex) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (buttonIndex == selectedAnswerIndex) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(null);
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
  }

  void validateNextPage() {
    if (questionIndex == allQuestions.length - 1) {
      setState(() {
        questionFlag = false;
      });
    }
    if (selectedAnswerIndex == -1) {
      return;
    }
    if (selectedAnswerIndex == allQuestions[questionIndex].answerIndex) {
      correctAnswer++;
    }
    if (selectedAnswerIndex != -1) {
      setState(() {
        questionIndex++;
        selectedAnswerIndex = -1;
      });
    }
  }

  Scaffold isQuestionScrren() {
    if (questionFlag == true) {
      return Scaffold(
        drawer: Drawer(
          child: ListView(
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(color: Colors.blue),
                child: Text('Drawer header'),
              ),
              ListTile(
                leading: const Icon(Icons.home),
                title: const Text('Settings'),
                onTap: () {},
              )
            ],
          ),
        ),

        appBar: AppBar(
          title: Text(
            "Quiz App",
            style: GoogleFonts.kalam(
              fontSize: 35,
              fontWeight: FontWeight.w400,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.yellow[400],
          actions: [
            IconButton(
              onPressed: () {},
              icon: ClipRRect(
                borderRadius: BorderRadius.circular(70),
                child: Image.asset(
                  'assets/PROFILE.jpg',
                  height: 70,
                  width: 40,
                ),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
          ],
        ),
        backgroundColor: Colors.purple[100],
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 15,
                  ),
                  Container(
                    height: 35,
                    width: 200,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "Questions : ",
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          "${questionIndex + 1}/${allQuestions.length}",
                          style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 7, right: 7),
                    child: Text(
                      textAlign: TextAlign.center,
                      "${allQuestions[questionIndex].question}",
                      style: GoogleFonts.kalam(
                        fontSize: 25,
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                        top: 20, bottom: 20, left: 10, right: 10),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          width: 340,
                          height: 50,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: checkAnswer(0),
                              foregroundColor:
                                  const MaterialStatePropertyAll(Colors.purple),
                            ),
                            onPressed: () {
                              if (selectedAnswerIndex == -1) {
                                setState(() {
                                  selectedAnswerIndex = 0;
                                });
                              }
                            },
                            child: Text(
                              "A.${allQuestions[questionIndex].options![0]}",
                              style: GoogleFonts.signika(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          width: 340,
                          height: 50,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: checkAnswer(1),
                              foregroundColor:
                                  const MaterialStatePropertyAll(Colors.purple),
                            ),
                            onPressed: () {
                              if (selectedAnswerIndex == -1) {
                                setState(() {
                                  selectedAnswerIndex = 1;
                                });
                              }
                            },
                            child: Text(
                              "B.${allQuestions[questionIndex].options![1]}",
                              style: GoogleFonts.signika(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          width: 340,
                          height: 50,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: checkAnswer(2),
                              foregroundColor:
                                  const MaterialStatePropertyAll(Colors.purple),
                            ),
                            onPressed: () {
                              if (selectedAnswerIndex == -1) {
                                setState(() {
                                  selectedAnswerIndex = 2;
                                });
                              }
                            },
                            child: Text(
                              "C.${allQuestions[questionIndex].options![2]}",
                              style: GoogleFonts.signika(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          width: 340,
                          height: 50,
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: checkAnswer(3),
                              foregroundColor:
                                  const MaterialStatePropertyAll(Colors.purple),
                            ),
                            onPressed: () {
                              if (selectedAnswerIndex == -1) {
                                setState(() {
                                  selectedAnswerIndex = 3;
                                });
                              }
                            },
                            child: Text(
                              "D.${allQuestions[questionIndex].options![3]}",
                              style: GoogleFonts.signika(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white70,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          height: 35,
                          width: 150,
                          child: Center(
                            child: Text(
                              'Score : $correctAnswer/${allQuestions.length}',
                              style: const TextStyle(fontSize: 20),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          splashColor: Colors.yellow,
          onPressed: () {
            validateNextPage();
          },
          tooltip: "Go to Next Question",
          backgroundColor: Colors.purple[300],
          child: const Icon(
            size: 40,
            Icons.arrow_forward,
            color: Colors.black,
          ),
        ),
        bottomNavigationBar: SalomonBottomBar(
          //margin: const EdgeInsets.all(8),
          backgroundColor: Colors.yellow[400],
          currentIndex: navBarIndex,
          onTap: (i) => setState(() => navBarIndex = i),
          items: [
            /// home
            SalomonBottomBarItem(
              icon: IconButton(
                iconSize: 30,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomePage(),
                    ),
                  );
                },
                icon: const Icon(Icons.home),
              ),
              title: const Text("Home"),
              selectedColor: Colors.purple,
            ),

            /// Search
            SalomonBottomBarItem(
              icon: IconButton(
                iconSize: 30,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ShimmerQuiz(),
                    ),
                  );
                },
                icon: const Icon(Icons.school_rounded),
              ),
              title: const Text("Learn"),
              selectedColor: Colors.purple,
            ),

            /// Profile
            SalomonBottomBarItem(
              icon: IconButton(
                iconSize: 30,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const Profile(),
                    ),
                  );
                },
                icon: const Icon(Icons.person),
              ),
              title: const Text("Profile"),
              selectedColor: Colors.green,
            ),
          ],
        ),

        // bottomNavigationBar: ConvexAppBar(
        //   height: 60,
        //   backgroundColor: Colors.black,
        //   items: [
        //     const TabItem(icon: Icons.quiz_outlined, title: 'Quiz'),
        //     const TabItem(icon: Icons.notes_outlined, title: 'Notes'),
        //     TabItem(
        //       // title: 'Home',
        //       icon: IconButton(
        //         iconSize: 40,
        //         tooltip: 'Go to Home Page',
        //         onPressed: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(
        //               builder: (context) => ShimmerQuiz(),
        //             ),
        //           );
        //         },
        //         icon: const Icon(
        //           Icons.home_filled,
        //         ),
        //       ),
        //     ),
        //     const TabItem(icon: Icons.message, title: 'Message'),
        //     const TabItem(icon: Icons.people, title: 'Profile'),
        //   ],
        //   onTap: (int i) => print(
        //     'click index=${i + 1}',
        //   ),
        // ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(
            "QuizApp",
            style: GoogleFonts.kalam(
              fontSize: 35,
              fontWeight: FontWeight.w400,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.yellow[400],
        ),
        backgroundColor: Colors.purple[100],
        body: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.purple[100],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  child: Image.asset("assets/bg.png"),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "Well Done !",
                  style: GoogleFonts.greatVibes(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 3,
                    color: Colors.deepPurple,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  textAlign: TextAlign.center,
                  "  You Have Completed The Quiz Successfully.",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.deepPurple),
                  ),
                  child: Column(
                    children: [
                      const Text(
                        "  Your Have Scored ",
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        "$correctAnswer/${allQuestions.length}",
                        style: const TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Container(
                  decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.yellow,
                        offset: Offset(9, 3),
                        blurRadius: 15,
                      ),
                    ],
                  ),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll(Colors.purple[400]),
                    ),
                    onPressed: () {
                      selectedAnswerIndex = -1;
                      questionIndex = 0;
                      questionFlag = true;
                      correctAnswer = 0;
                      setState(() {});
                    },
                    child: const Text(
                      "Reset Quiz",
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        bottomNavigationBar: SalomonBottomBar(
          margin: const EdgeInsets.all(15),
          backgroundColor: Colors.yellow[400],
          currentIndex: navBarIndex,
          onTap: (i) => setState(() => navBarIndex = i),
          items: [
            /// Home
            SalomonBottomBarItem(
              icon: IconButton(
                iconSize: 30,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomePage(),
                    ),
                  );
                },
                icon: const Icon(Icons.home),
              ),
              title: const Text("Home"),
              selectedColor: Colors.purple,
            ),

            /// Search
            SalomonBottomBarItem(
              icon: IconButton(
                iconSize: 30,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ShimmerQuiz(),
                    ),
                  );
                },
                icon: const Icon(Icons.school_rounded),
              ),
              title: const Text("Learn"),
              selectedColor: Colors.purple,
            ),

            /// Profile
            SalomonBottomBarItem(
              icon: IconButton(
                iconSize: 30,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const Profile(),
                    ),
                  );
                },
                icon: const Icon(Icons.person),
              ),
              title: const Text("Profile"),
              selectedColor: Colors.green,
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScrren();
  }
}
