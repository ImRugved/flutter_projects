import 'dart:developer';

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: InheritedDemo(),
    );
  }
}

class InheritedDemo extends StatefulWidget {
  const InheritedDemo({super.key});

  @override
  State createState() => _InheritedDemoState();
}

class _InheritedDemoState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Inherited Widget'),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: const Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Course(name: 'flutter'),
          SizedBox(
            height: 20,
          ),
          Course(name: 'java'),
          SizedBox(
            height: 20,
          ),
          Course(name: 'Python')
        ],
      ),
    );
  }
}

class Course extends StatefulWidget {
  final String name;
  const Course({super.key, required this.name});

  @override
  State<Course> createState() => _CourseState();
}

class _CourseState extends State<Course> {
  int refCount = 0;
  @override
  Widget build(BuildContext context) {
    log('in coursestae build');
    log(widget.name);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              refCount++;
            });
          },
          child: Container(
            alignment: Alignment.center,
            height: 50,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.yellow,
              borderRadius: BorderRadius.circular(
                15,
              ),
            ),
            child: Text(widget.name),
          ),
        ),
        const SizedBox(
          width: 15,
        ),
        Container(
          alignment: Alignment.center,
          height: 30,
          width: 80,
          decoration: BoxDecoration(
            color: Colors.purple,
            borderRadius: BorderRadius.circular(
              15,
            ),
          ),
          child: Text(
            'count is $refCount',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    );
  }
}
