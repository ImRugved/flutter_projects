import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:todo/listViewBuildeCode.dart';

class TaskModel {
  final String title;
  final String description;
  final String date;

  TaskModel({
    required this.title,
    required this.description,
    required this.date,
  });
}

class TodoList extends StatefulWidget {
  const TodoList({super.key});

  @override
  State<TodoList> createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  TextEditingController titleController = TextEditingController();

  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  List cardColor = [
    const Color.fromRGBO(250, 232, 232, 100),
    const Color.fromRGBO(232, 237, 250, 100),
    const Color.fromRGBO(250, 249, 232, 100),
    const Color.fromRGBO(250, 232, 250, 100),
  ];

  final List<TaskModel> tasks = [
    TaskModel(
      title: 'Buy groceries',
      description: "Go to the store and buy all the things",
      date: '2021-12-31',
    ),
    TaskModel(
      title: 'Buy groceries',
      description: "Go to the store and buy all the things",
      date: '2021-12-31',
    ),
    TaskModel(
      title: 'Buy groceries',
      description: "Go to the store and buy all the things",
      date: '2021-12-31',
    ),
    TaskModel(
      title: 'Buy groceries',
      description: "Go to the store and buy all the things",
      date: '2021-12-31',
    ),
    TaskModel(
      title: 'Buy groceries',
      description: "Go to the store and buy all the things",
      date: '2021-12-31',
    ),
  ];

  void _addTaskToList() {
    setState(() {
      String enteredTitle = titleController.text.trim();
      String enteredDescription = descriptionController.text.trim();
      String enteredDate = dateController.text.trim();

      if (enteredTitle.isNotEmpty &&
          enteredDescription.isNotEmpty &&
          enteredDate.isNotEmpty) {
        tasks.add(
          TaskModel(
            title: enteredTitle,
            description: enteredDescription,
            date: enteredDate,
          ),
        );

        // Clear text controllers
        titleController.clear();
        descriptionController.clear();
        dateController.clear();
      }
    });
  }

  // void _editTask(TaskModel task, int index) {
  //   // Set initial values for text controllers
  //   titleController.text = task.title;
  //   descriptionController.text = task.description;
  //   dateController.text = task.date;

  //   // Show bottom sheet with editing functionality
  //   showModalBottomSheet(
  //     isScrollControlled: true,
  //     context: context,
  //     builder: (BuildContext context) {
  //       return Padding(
  //         padding: MediaQuery.of(context).viewInsets,
  //         //padding: const EdgeInsets.all(10),
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           mainAxisSize: MainAxisSize.min,
  //           children: [
  //             Text(
  //               'Create Task',
  //               style: GoogleFonts.quicksand(
  //                 textStyle: const TextStyle(
  //                   fontWeight: FontWeight.w600,
  //                   fontSize: 22,
  //                 ),
  //               ),
  //             ),
  //             const SizedBox(
  //               height: 15,
  //             ),
  //             Column(
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               children: [
  //                 const Text(
  //                   'Title',
  //                   style: TextStyle(
  //                       color: Color.fromRGBO(1, 177, 167, 2),
  //                       fontSize: 15,
  //                       fontWeight: FontWeight.w400),
  //                 ),
  //                 SizedBox(
  //                   width: 600,
  //                   child: TextField(
  //                     controller: titleController,
  //                     keyboardType: TextInputType.text,
  //                     decoration: InputDecoration(
  //                       border: OutlineInputBorder(
  //                         borderRadius: BorderRadius.circular(10),
  //                         borderSide: const BorderSide(
  //                           color: Color.fromRGBO(1, 177, 167, 2),
  //                         ),
  //                       ),
  //                     ),
  //                     cursorColor: Colors.black,
  //                     textInputAction: TextInputAction.done,
  //                   ),
  //                 ),
  //                 const SizedBox(height: 20),
  //                 const Text(
  //                   'Description',
  //                   style: TextStyle(
  //                       color: Color.fromRGBO(1, 177, 167, 2),
  //                       fontSize: 15,
  //                       fontWeight: FontWeight.w400),
  //                 ),
  //                 SizedBox(
  //                   width: 600,
  //                   child: TextField(
  //                     controller: descriptionController,
  //                     keyboardType: TextInputType.text,
  //                     decoration: InputDecoration(
  //                       border: OutlineInputBorder(
  //                         borderRadius: BorderRadius.circular(10),
  //                         borderSide: const BorderSide(
  //                           color: Color.fromRGBO(1, 177, 167, 2),
  //                         ),
  //                       ),
  //                     ),
  //                     cursorColor: Colors.black,
  //                     textInputAction: TextInputAction.done,
  //                   ),
  //                 ),
  //                 const SizedBox(height: 20),
  //                 const Text(
  //                   'Date',
  //                   style: TextStyle(
  //                       color: Color.fromRGBO(1, 177, 167, 2),
  //                       fontSize: 15,
  //                       fontWeight: FontWeight.w400),
  //                 ),
  //                 SizedBox(
  //                   width: 600,
  //                   child: TextField(
  //                       controller: dateController,
  //                       keyboardType: TextInputType.text,
  //                       decoration: InputDecoration(
  //                         border: OutlineInputBorder(
  //                           borderRadius: BorderRadius.circular(10),
  //                           borderSide: const BorderSide(
  //                             color: Color.fromRGBO(1, 177, 167, 2),
  //                           ),
  //                         ),
  //                         fillColor: const Color.fromRGBO(1, 177, 167, 2),
  //                         suffix: const Icon(Icons.calendar_month_outlined),
  //                       ),
  //                       cursorColor: Colors.black,
  //                       textInputAction: TextInputAction.done,
  //                       readOnly: true,
  //                       onTap: () async {
  //                         DateTime? pickedDate = await showDatePicker(
  //                           context: context,
  //                           initialDate: DateTime.now(),
  //                           firstDate: DateTime(2024),
  //                           lastDate: DateTime(2025),
  //                         );
  //                         String formatDate = DateFormat.yMd().format(
  //                           pickedDate!,
  //                         );
  //                         setState(() {
  //                           dateController.text = formatDate;
  //                         });
  //                       }),
  //                 ),
  //               ],
  //             ),
  //             const SizedBox(height: 25),
  //             SizedBox(
  //               width: 300,
  //               height: 50,
  //               child: ElevatedButton(
  //                 style: const ButtonStyle(
  //                   backgroundColor: MaterialStatePropertyAll(
  //                     Color.fromRGBO(1, 177, 167, 2),
  //                   ),
  //                   shape: MaterialStatePropertyAll(
  //                     RoundedRectangleBorder(
  //                       borderRadius: BorderRadius.all(
  //                         Radius.circular(10),
  //                       ),
  //                     ),
  //                   ),
  //                 ),
  //                 onPressed: () {
  //                   setState(() {
  //                     tasks[index] = TaskModel(
  //                       title: titleController.text,
  //                       description: descriptionController.text,
  //                       date: dateController.text,
  //                     );
  //                   });
  //                   titleController.clear();
  //                   descriptionController.clear();
  //                   dateController.clear();
  //                   Navigator.of(context).pop();
  //                 },
  //                 child: Text(
  //                   'Submit',
  //                   style: GoogleFonts.inter(
  //                     textStyle: const TextStyle(
  //                       fontWeight: FontWeight.w700,
  //                       fontSize: 20,
  //                       color: Colors.white,
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             )
  //           ],
  //         ),
  //       );
  //     },
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(
          2,
          167,
          177,
          1,
        ),
        title: const Text(
          'To-Do List',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 26,
          ),
        ),
      ),
      //body: ListViewBuilderCode(),
      // body: Padding(
      //   padding: const EdgeInsets.symmetric(
      //     vertical: 15,
      //     horizontal: 15,
      //   ),
      //   child: (tasks.isEmpty)
      //       ? Center(
      //           child: Text(
      //             'No Tasks available add new task',
      //             style: GoogleFonts.quicksand(
      //               textStyle: const TextStyle(
      //                 fontWeight: FontWeight.bold,
      //                 fontSize: 22,
      //                 color: Color.fromRGBO(1, 177, 167, 2),
      //               ),
      //             ),
      //           ),
      //         )
      //       : ListView.builder(
      //           itemCount: tasks.length,
      //           itemBuilder: (context, index) => Container(
      //             padding: const EdgeInsets.all(10),
      //             margin: const EdgeInsets.only(bottom: 20),
      //             decoration: BoxDecoration(
      //               color: cardColor[index % cardColor.length],
      //               borderRadius: BorderRadius.circular(25),
      //             ),
      //             child: Column(
      //               children: [
      //                 Row(
      //                   children: [
      //                     CircleAvatar(
      //                       child: Text('${index + 1}'),
      //                     ),
      //                     const SizedBox(
      //                       width: 8,
      //                     ),
      //                     Container(
      //                       width: 80,
      //                       height: 80,
      //                       decoration: const BoxDecoration(
      //                         shape: BoxShape.circle,
      //                         color: Colors.white,
      //                       ),
      //                       child: const Icon(Icons.image),
      //                     ),
      //                     const SizedBox(
      //                       width: 10,
      //                     ),
      //                     Expanded(
      //                       child: Column(
      //                         mainAxisAlignment: MainAxisAlignment.start,
      //                         crossAxisAlignment: CrossAxisAlignment.start,
      //                         children: [
      //                           Text(
      //                             tasks[index].title,
      //                             style: GoogleFonts.quicksand(
      //                               textStyle: const TextStyle(
      //                                 fontWeight: FontWeight.w600,
      //                                 fontSize: 20,
      //                               ),
      //                             ),
      //                           ),
      //                           const SizedBox(
      //                             height: 5,
      //                           ),
      //                           Text(
      //                             tasks[index].description,
      //                             style: GoogleFonts.quicksand(
      //                               textStyle: const TextStyle(
      //                                 fontWeight: FontWeight.w500,
      //                                 fontSize: 15,
      //                               ),
      //                             ),
      //                           )
      //                         ],
      //                       ),
      //                     ),
      //                   ],
      //                 ),
      //                 Row(
      //                   children: [
      //                     const SizedBox(
      //                       width: 10,
      //                     ),
      //                     Text(
      //                       tasks[index].date,
      //                       style: GoogleFonts.quicksand(
      //                         textStyle: const TextStyle(
      //                           fontWeight: FontWeight.w500,
      //                           fontSize: 15,
      //                         ),
      //                       ),
      //                     ),
      //                     const Spacer(),
      //                     Row(
      //                       children: [
      //                         IconButton(
      //                           onPressed: () {
      //                             _editTask(tasks[index], index);
      //                           },
      //                           icon: const Icon(
      //                             Icons.edit_outlined,
      //                             color: Color.fromRGBO(
      //                               2,
      //                               167,
      //                               177,
      //                               1,
      //                             ),
      //                           ),
      //                         ),
      //                         IconButton(
      //                           onPressed: () {
      //                             setState(() {
      //                               // Remove task from list and update UI
      //                               tasks.removeAt(index);
      //                             });
      //                           },
      //                           icon: const Icon(
      //                             Icons.delete_outline_rounded,
      //                             color: Color.fromRGBO(
      //                               2,
      //                               167,
      //                               177,
      //                               1,
      //                             ),
      //                           ),
      //                         ),
      //                       ],
      //                     ),
      //                   ],
      //                 )
      //               ],
      //             ),
      //           ),
      //         ),
      // ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        onPressed: () {
          showModalBottomSheet(
            isScrollControlled: true,
            context: context,
            builder: (BuildContext context) {
              return Padding(
                padding: MediaQuery.of(context).viewInsets,
                //padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Create Task',
                      style: GoogleFonts.quicksand(
                        textStyle: const TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Title',
                          style: TextStyle(
                              color: Color.fromRGBO(1, 177, 167, 2),
                              fontSize: 15,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          width: 600,
                          child: TextField(
                            controller: titleController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Color.fromRGBO(1, 177, 167, 2),
                                ),
                              ),
                            ),
                            cursorColor: Colors.black,
                            textInputAction: TextInputAction.done,
                          ),
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          'Description',
                          style: TextStyle(
                              color: Color.fromRGBO(1, 177, 167, 2),
                              fontSize: 15,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          width: 600,
                          child: TextField(
                            controller: descriptionController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Color.fromRGBO(1, 177, 167, 2),
                                ),
                              ),
                            ),
                            cursorColor: Colors.black,
                            textInputAction: TextInputAction.done,
                          ),
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          'Date',
                          style: TextStyle(
                              color: Color.fromRGBO(1, 177, 167, 2),
                              fontSize: 15,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          width: 600,
                          child: TextField(
                              controller: dateController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                    color: Color.fromRGBO(1, 177, 167, 2),
                                  ),
                                ),
                                fillColor: const Color.fromRGBO(1, 177, 167, 2),
                                suffix:
                                    const Icon(Icons.calendar_month_outlined),
                              ),
                              cursorColor: Colors.black,
                              textInputAction: TextInputAction.done,
                              readOnly: true,
                              onTap: () async {
                                DateTime? pickedDate = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(2024),
                                  lastDate: DateTime(2025),
                                );
                                String formatDate = DateFormat.yMd().format(
                                  pickedDate!,
                                );
                                setState(() {
                                  dateController.text = formatDate;
                                });
                              }),
                        ),
                      ],
                    ),
                    const SizedBox(height: 25),
                    SizedBox(
                      width: 300,
                      height: 50,
                      child: ElevatedButton(
                        style: const ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(
                            Color.fromRGBO(1, 177, 167, 2),
                          ),
                          shape: MaterialStatePropertyAll(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                          ),
                        ),
                        onPressed: () {
                          _addTaskToList();
                          Navigator.of(context).pop();
                          titleController.clear();
                          descriptionController.clear();
                          dateController.clear();
                        },
                        child: Text(
                          'Submit',
                          style: GoogleFonts.inter(
                            textStyle: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          );
        },
        child: const Icon(
          Icons.add,
        ),
      ),
    );
  }
}
